const gulp = require('gulp');
const plumber = require('gulp-plumber');
const del = require('del');
const gulpPug = require('gulp-pug');
const prettyHtml = require('gulp-pretty-html');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const gulpStylelint = require('gulp-stylelint');
const autoprefixer = require('gulp-autoprefixer');
const csso = require('gulp-csso');
const sourcemaps = require('gulp-sourcemaps');
const zip = require('gulp-zip');
const browserSync = require('browser-sync').create();
const stream = require('webpack-stream');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config.js');
const webpackConfigProd = require('./webpack.config.prod.js');
const packageJson = require('./package.json');

sass.compiler = require('node-sass');

const config = {
  dist: './dist',
  zip: './zip',
  pug: {
    pages: './app/pages/*.pug',
    src: './app/**/*.pug',
    dist: './dist/',
  },
  styles: {
    main: './app/assets/styles/main.scss',
    src: './app/**/*.scss',
    dist: './dist/assets/css',
  },
  scripts: {
    src: './app/**/*.js',
    entry: './app/assets/js/index.js',
    dist: './dist/assets/js/',
  },
  images: {
    src: './app/assets/images/**/*.*',
    dist: './dist/assets/images/',
  },
  fonts: {
    src: './app/assets/fonts/**/*.*',
    dist: './dist/assets/fonts/',
  },
  staticFiles: {
    src: './app/static/**/*.*',
    dist: './dist/',
  },
};

// Cleaning up the dist folder
const clean = () => del(config.dist);

// Cleaning up the zip folder
const cleanZip = () => del(config.zip);

// Copy assets
const images = () => gulp.src(config.images.src, {
  since: gulp.lastRun(images),
})
  .pipe(gulp.dest(config.images.dist));

// Copy fonts
const fonts = () => gulp.src(config.fonts.src, {
  since: gulp.lastRun(fonts),
})
  .pipe(gulp.dest(config.fonts.dist));

// Copy static folder
const staticFiles = () => gulp.src(config.staticFiles.src, {
  since: gulp.lastRun(staticFiles),
})
  .pipe(gulp.dest(config.staticFiles.dist));

// Pug
const pug = () => gulp.src(config.pug.pages)
  .pipe(plumber())
  .pipe(gulpPug())
  .pipe(prettyHtml({
    indent_size: 2,
    indent_char: ' ',
  }))
  .pipe(gulp.dest(config.dist));

// Styles
const styles = () => gulp.src(config.styles.main)
  .pipe(plumber())
  .pipe(sourcemaps.init())
  .pipe(gulpStylelint({
    reporters: [
      { formatter: 'string', console: true },
    ],
  }))
  .pipe(sassGlob())
  .pipe(sass({
    outputStyle: 'compressed',
  }).on('error', sass.logError))
  .pipe(autoprefixer())
  .pipe(csso())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(config.styles.dist))
  // .pipe(browserSync.stream());

// Styles Production
const stylesProd = () => gulp.src(config.styles.main)
  .pipe(plumber())
  .pipe(gulpStylelint({
    reporters: [
      { formatter: 'string', console: true },
    ],
  }))
  .pipe(sassGlob())
  .pipe(sass({
    outputStyle: 'compressed',
  }).on('error', sass.logError))
  .pipe(autoprefixer())
  .pipe(csso())
  .pipe(gulp.dest(config.styles.dist));

// Scripts
const scripts = () => gulp.src(config.scripts.entry)
  .pipe(stream(webpackConfig, webpack))
  .pipe(gulp.dest(config.scripts.dist));

// Scripts Prod
const scriptsProd = () => gulp.src(config.scripts.entry)
  .pipe(stream(webpackConfigProd, webpack))
  .pipe(gulp.dest(config.scripts.dist));

// Watch
const watch = () => {
  gulp.watch(config.images.src, images);
  gulp.watch(config.fonts.src, fonts);
  gulp.watch(config.staticFiles.src, staticFiles);
  gulp.watch(config.styles.src, styles);
  gulp.watch(config.pug.src, pug);
  gulp.watch(config.scripts.src, scripts);
};

// Server
const server = () => {
  browserSync.init({
    server: config.dist,
  });
  browserSync.watch(`${config.dist}/**/*.*`, browserSync.reload);
};

// Zip arhive
const archive = () => {
  const project = packageJson;
  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const dd = (day < 10) ? `0${day}` : day;
  const mm = (month < 10) ? `0${month}` : month;
  const yyyy = date.getFullYear();
  const today = `${yyyy}.${mm}.${dd}`;

  const projectName = project.name;

  return gulp.src(`${config.dist}/**`)
    .pipe(zip(`${projectName}-${today}.zip`))
    .pipe(gulp.dest(config.zip));
};

exports.default = gulp.series(
  clean,
  gulp.parallel(pug, styles, scripts, fonts, staticFiles, images),
  gulp.parallel(watch, server),
);

exports.build = gulp.series(
  clean,
  gulp.parallel(pug, stylesProd, scriptsProd, fonts, staticFiles, images),
  cleanZip,
  archive,
);
