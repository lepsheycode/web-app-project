# Стартовый проект с gulp

# RU

## Описание

- Именование классов по БЭМ, разметка в [pug](https://pugjs.org/), стилизация [SCSS](https://sass-scss.ru/guide//).
- Каждый БЭМ-блок в своей папке внутри `app/components/`. Один БЭМ-блок — один pug-файл, один scss-файл, один js-файл.
- Есть глобальные файлы: стилевые, js, шрифты, картинки.
- Диспетчер подключения стилей `app/assets/styles/main.scss` содержит импорты стилевых файлов использованных в разметке компонент и импорты доп. файлов.
- Входная точка обработки js (`app/assets/js/index.js`) содержит `import` js-файлов использованных в разметке компонентов и доп. файлов.


## Команды

```bash
yarn start      # запуск сервера разработки
yarn build      # сборка проект и создание zip архива
yarn deploy     # отправка содержимого папки сборки
```


## Структура

```bash
app/                # Исходники
  assets/           # Ассеты
    fonts/          # Шрифты
    images/         # Изображения
      favicon/      # Фавиконки
    js/             # Общие файлы js, точка входа index.js
    styles/         # Общие файлы стилей

  components/       # Компоненты проекта
  layouts/          # Шаблоны страниц проекта
  pages/            # Страницы проекта
  static/           # Статические файлы (при сборке лежат в корне)
dist/               # Папка сборки
gulpfile.js         # Настройка сборки и деплоя проекта
```

## Компоненты

Каждый компонент лежит в `app/components/` в своей папке.

Возможное содержимое компонента:

```bash
demo-component/        # Папка компонента.
  demo-component.pug   # Разметка (pug-миксин, отдающая разметку этого компонента, описание API примеси).
  demo-component.scss  # Стилевой файл этого компонента (без стилей других компонентов).
  demo-component.js    # js-файл компонента (без скриптов других компонентов).
```

## Сторонние npm-модули

Если нужно взять в сборку сторонний модуль, то после его установки:

1. Прописать `import` в js-файле проектного компонента (там же писать всё, что касается работы с этим модулем).
2. Если нужно брать в сборку стилевые файлы модуля, прописать их `@import` в файле `app/assets/styles/base/libs.scss`

## Разметка

Используется [pug](https://pugjs.org/api/getting-started.html).

Все страницы (см. `app/pages/`) являются расширениями шаблонов из `app/layouts` (см. [наследование шаблонов](https://pugjs.org/language/inheritance.html)), в страницах описывается только содержимое контентной области

Каждый компонент (в `app/components/`) может содержать одноимённый pug-файл с одноименным миксином

Все компоненты необходимо инклюдить (в `app/layouts/default.pug`)

## Стили

Все стили подключаются в (`app/assets/styles/main.scss`)

Каждый компонент (в `app/components/`) может содержать одноимённый scss-файл со стилями этого компонента. Если компонент используется в разметке, scss-файл будет взят в сборку стилей.

## Скрипты

Точка входа (`app/assets/js/index.js`). Точка входа обрабатывается webpack-ом.

Для глобальных действий предусмотрен `app/assets/js/main.js`.

Каждый компонент (в `app/components/`) может содержать одноимённый js-файл.


# EN

Node.js > v10.x
npm install gulp-cli -g  // Install the latest Gulp CLI tools globally

## Description

- Class naming BEM, [pug](https://pugjs.org/) layout, stylization [Stylus](http://stylus-lang.com/).
- Each BEM-block is in appropriate folder inside `app/components/`. One BEM-block — one pug-file, one styl-file, one js-file.
- There are global files: styles, js, fonts, images.
- Style Connection Manager `app/assets/styles/main.styl` contains imports of style files used in component layouts and additional import files.
- Processing Entry Point js (`app/assets/js/index.js`) contains `import` js-files used in in the  components layouts and additional files.


## Commands

```bash
npm i / yarn                    # install dependies
yarn start / npm run start      # run development server
yarn build  / npm run build     # project build and zip archive creation
yarn deploy / npm run deploy    # sending assemble folder contents
```


## Structure

```bash
app/                # Source code
  assets/           # Assets
    fonts/          # Fonts
    images/         # Images
      favicon/      # Favicons
    js/             # Js, entry index.js
    styles/         # Common style files

  components/       # Project components
  layouts/          # Project pages layouts
  pages/            # Project pages
  static/           # Static files (robots.txt etc.)
dist/               # Build folder
gulpfile.js         # Build configuration and project deployment
```

## Components

Each component lies at `app/components/` in its own folder.

Possible component contents:

```bash
demo-component/        # Component folder.
  demo-component.pug   # Layout (pug-mixin that provides layout of this component,  API description).
  demo-component.styl  # Style file of certain component (without styles of other components).
  demo-component.js    # js-file of the component (without scripts of other components).
```

## Third-party npm modules

If third-party module is needed to assemble,  after installing it:

1. Register `import` in the js-file of the project component (register there everything related to working with this module).
2. If module style files need to assemble, register them `@import` in `app/assets/styles/base/libs.styl`

## Layout

[Pug](https://pugjs.org/api/getting-started.html) is used.

App pages (`app/pages/`) are extensions of templates from `app/layouts` ([templates](https://pugjs.org/language/inheritance.html)), only content area describes on the pages.

Each component (in `app/components/`) can contain pug-file of the same name with the same name mixin.

All components have to be included  (в `app/layouts/default.pug`).

## Styles

All styles are connected in (`app/assets/styles/main.styl`)

Each component (in `app/components/`) can contain the same-name styl-file with the styles of this component. If the component is used in layout, its styl-file will be taken into the styles assemble.


## Scripts

Entry point (`app/assets/js/index.js`). Entry is processed by [rollup](https://rollupjs.org/guide/en/) (with rollup-plugin-babel).

For global actions `app/assets/js/main.js` is foreseen.

Each component (in `app/components/`) can contain same-name  js-file.
